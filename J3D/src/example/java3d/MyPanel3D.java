
package example.java3d;

import java.awt.*;
import javax.swing.*;
import javax.media.j3d.*;
import com.sun.j3d.utils.universe.*;

import com.sun.j3d.utils.geometry.*;
import javax.vecmath.*;

public class MyPanel3D extends JPanel
{

  private Canvas3D canvas;
  private SimpleUniverse universe;

  public MyPanel3D()
  {
     canvas = new Canvas3D (SimpleUniverse.getPreferredConfiguration ());
     universe = new SimpleUniverse (canvas);
     universe.getViewingPlatform().setNominalViewingTransform();

     this.setLayout (new BorderLayout ()); // important
     this.add (canvas, BorderLayout.CENTER);
  }

  /* ------------------------------------------------------------- */

  private BranchGroup content;

  public void setContent (BranchGroup newContent)
  {
     if (content != newContent)
     {
        if (content != null)
           try
           {
              content.detach ();
           }
           catch (Throwable e)
           {
           }

        content = newContent;

        if (content != null)
           universe.addBranchGraph (content);

        validate ();
     }
  }

  /* ------------------------------------------------------------- */

  public void addBranchGroup (BranchGroup bg)
  {
    universe.addBranchGraph (bg);
  }

  /* ------------------------------------------------------------- */

  public void addNode (Node node)
  {
    BranchGroup bg = new BranchGroup ();
    bg.addChild (node);
    universe.addBranchGraph (bg);
  }

  /* ------------------------------------------------------------- */

  public void addTransformedNode (Node node)
  {
     Transform3D trans = new Transform3D ();
     trans.setRotation (new AxisAngle4d (1.0, 1.0, 1.0, Math.PI/4));
     trans.setScale (0.2);

     TransformGroup tg = new TransformGroup ();
     tg.setTransform (trans);
     tg.addChild (node);

     BranchGroup bg = new BranchGroup ();
     bg.addChild (tg);
     universe.addBranchGraph (bg);
  }

  /* ------------------------------------------------------------- */

  void setBackground (float r, float g, float b)
  {
     BranchGroup bg = new BranchGroup ();
     Background background = new Background ();
     background.setColor (r, g, b);
     BoundingSphere bs = new BoundingSphere (new Point3d (), 100);
     background.setApplicationBounds (bs);
     universe.addBranchGraph (bg);
  }

}
