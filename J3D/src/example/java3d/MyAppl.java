
package example.java3d;

import javax.swing.UIManager;

public class MyAppl
{
  boolean packFrame = false;

  //Construct the application
  public MyAppl()
  {
    MyWindow frame = new MyWindow();
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (packFrame)
      frame.pack();
    else
      frame.validate();
    frame.setVisible(true);
  }

  //Main method
  public static void main(String[] args)
  {
    try 
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch(Exception e)
    {
    }
    new MyAppl();
  }
} 