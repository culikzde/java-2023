
package example.java3d;

import javax.media.j3d.*;
import javax.vecmath.*;

public class Dodecahedron extends Shape3D
{

  /* ------------------------------------------------------------- */

  private double [] s = new double [10]; // tabulka sin
  private double [] c = new double [10]; // tabulka cos

  private void calcSinCos ()
  {
     for (int i = 0; i < 10; i++)
     {
        double u = 2 * Math.PI * i / 10;
        s[i] = Math.sin (u);
        c[i] = Math.cos (u);
     }
  }

  /* ------------------------------------------------------------- */

  private Point3f [] verts = new Point3f [32];
  private Color3f [] colors = new Color3f [32];

  int [] pntsIndex = new int [84];
  int [] clrsIndex = new int [84];

  private int vert_cnt;
  private int pnts_cnt;

 /* ------------------------------------------------------------- */

  private static final int A =  0; // prvni index vrcolu v hornim prstenci
  private static final int B =  5;
  private static final int C = 10;
  private static final int D = 15; // prvni index vrcolu v dolnim prstenci

  private void circle (double r, double v, int shift)
  {
     // vrcholy v jedne vrstve

     Point3f [] tab = new Point3f [5];

     for (int i = 0; i < 5; i++)
     {
        int inx = 2*i + shift;
        double x = r * c[inx];
        double y = v;
        double z = r * s[inx];

        verts [vert_cnt] = new Point3f ((float)x, (float)y, (float)z);
        vert_cnt ++;
     }
  }

  /* ------------------------------------------------------------- */

  int side_cnt;

  private void side (int i1, int i2, int i3, int i4, int i5)
  {
     // indexy vrcholu petiuhelniku
     // obracene poradi, aby byly videt vnejsi steny
     int [] tab = new int [] { i5, i4, i3, i2, i1 };
     // int [] tab = new int [] { i1, i2, i3, i4, i5 };

     // stred petiuhelniku a prumerna barva
     Point3f sum = new Point3f ();
     Color3f clr = new Color3f ();

     for (int i = 0; i < 5; i++)
     {
        int inx = tab [i];
        sum.add (verts [inx]);
        clr.add (colors [inx]);
     }
     sum.scale (0.2f);
     clr.scale (0.2f);

     // ulozit souradnice a barvu stredu petiuhelniku
     int sum_inx = vert_cnt;
     verts [vert_cnt] = sum;
     colors [vert_cnt] = clr;
     vert_cnt ++;

     // ulozit indexy vrcholu
     int k = pnts_cnt;
     pntsIndex [k] = sum_inx;  // stred
     for (int i = 0; i < 5; i++)
        pntsIndex [k+1+i] = tab [i]; // jednotlive vrcholy
     pntsIndex [k+6] = tab [0]; // znovu prvni vrchol
     pnts_cnt += 7;

     // barva steny
     for (int i = 0; i < 7; i++)
        clrsIndex [k+i] = side_cnt;

     side_cnt ++;

     System.out.println ("petiuhelnik " + side_cnt);
     for (int i = 0; i < 5; i++)
     {
        k = (i+1) % 5;
        Point3f p = verts [tab [i]];
        Point3f q = verts [tab [k]];
        System.out.println ("vrchol [" + i + "] = " + p +
                            ", vzdalenost = " + p.distance (new Point3f ()) +
                            ", delta = " + p.distance (q));
     }

     Point3f p = verts [sum_inx];
     System.out.println ("stred  = " + p +
                         ", vzdalenost = " + p.distance (new Point3f ()) );
  }

  /* ------------------------------------------------------------- */

  private void calc ()
  {
     double fi = 2 * Math.PI / 10;      // 36 stupnu

     double a = 1.0;                    // delka hrany
     double c = 2 * a * Math.cos (fi);  // delka hrany vetsiho prstence
     double b = c / Math.sqrt (3.0);
     double v = Math.sqrt (a*a-b*b);
     double R = a*a / (2*v);            // polomer opsane koule

     double ra = a / (2*Math.sin(fi));  // polomer mensiho prstence
     double va = Math.sqrt (R*R-ra*ra); // vyska mensiho prstence nad rovnikem

     double rc = c / (2*Math.sin(fi));  // polomer vetsiho prstence
     double vc = Math.sqrt (R*R-rc*rc); // vyska vetsiho prstence nad rovnikem

     // tabulka sin a cos
     calcSinCos ();

     // souradnice vrcholu
     vert_cnt = 0;
     circle (ra, va, 0);  // mensi prstenec nad rovnikem, indexy  0..4  (A)
     circle (rc, vc, 0);  // vetsi prstenec nad rovnikem, indexy  5..9  (B)
     circle (rc, -vc, 1); // vetsi prstenec pod rovnikem, indexy 10..14 (C)
     circle (ra, -va, 1); // mensi prstenec pod rovnikem, indexy 15..19 (D)

     pnts_cnt = 0;
     side_cnt = 0;

     // horni stena
     side (A+0, A+1, A+2, A+3, A+4);

     // skupina hornich sten
     for (int i = 0; i < 5; i++)
     {
        int k = (i+1) % 5;
        side (A+i, B+i, C+i, B+k, A+k);
        // jeden bod z prstence A, B, C a sousedni body z prstence B, A
     }

     // skupina 5 dolnich sten
     for (int i = 0; i < 5; i++)
     {
        int k = (i+1) % 5;
        side (D+k, C+k, B+k, C+i, D+i);
        // jeden bod z prstence D, C, B a sousedni body z prstence C, D
     }

     // dolni stena
     side (D+4, D+3, D+2, D+1, D+0);

     /*
     double d = 2 * rc * Math.sin (fi/2);
     double dd = rc * Math.sqrt (2 * (1-Math.cos (fi)));
     double h = Math.sqrt (a*a-d*d);
     double vc_alt = h/2;
     double R_alt = Math.sqrt (rc*rc + vc_alt*vc_alt);

     double x = rc-ra;
     double y = va-vc;
     double a_alt = Math.sqrt (x*x + y*y);
     */
  }

  private static final int[] sVertCnt =
  { 7, 7, 7,   7, 7, 7,   7, 7, 7,   7, 7, 7};

  public Dodecahedron ()
  {
     IndexedTriangleFanArray obj =
        new IndexedTriangleFanArray (20+12, GeometryArray.COORDINATES | GeometryArray.COLOR_3, 12*7, sVertCnt);

     // dvacet barev musi byt nadefinovano pred vypoctem
     colors [ 0] = new Color3f (1.0f, 0.0f, 0.0f);
     colors [ 1] = new Color3f (0.0f, 1.0f, 0.0f);
     colors [ 2] = new Color3f (0.0f, 0.0f, 1.0f);
     colors [ 3] = new Color3f (1.0f, 1.0f, 0.0f);
     colors [ 4] = new Color3f (1.0f, 0.0f, 1.0f);
     colors [ 5] = new Color3f (0.0f, 1.0f, 1.0f);

     colors [ 6] = new Color3f (1.0f, 0.8f, 0.0f);
     colors [ 7] = new Color3f (0.0f, 1.0f, 0.8f);
     colors [ 8] = new Color3f (0.8f, 0.0f, 1.0f);
     colors [ 9] = new Color3f (1.0f, 0.0f, 0.8f);
     colors [10] = new Color3f (0.8f, 1.0f, 0.0f);
     colors [11] = new Color3f (0.0f, 0.8f, 1.0f);
     colors [12] = new Color3f (1.0f, 1.0f, 0.8f);
     colors [13] = new Color3f (1.0f, 0.8f, 1.0f);
     colors [14] = new Color3f (0.8f, 1.0f, 1.0f);

     colors [15] = new Color3f (1.0f, 0.5f, 0.5f);
     colors [16] = new Color3f (0.5f, 1.0f, 0.5f);
     colors [17] = new Color3f (0.5f, 0.5f, 1.0f);

     colors [18] = new Color3f (0.5f, 0.0f, 0.0f);
     colors [19] = new Color3f (0.0f, 0.5f, 0.0f);

     calc ();


     /*
     // duhove barvy
     for (int i = 0; i < 84; i++)
        clrsIndex [i] = pntsIndex [i];
     */

     obj.setCoordinates (0, verts);
     obj.setCoordinateIndices (0, pntsIndex);
     obj.setColors (0, colors);
     obj.setColorIndices (0, clrsIndex);

     this.setGeometry (obj);

     // Util.info (verts);
     // polomer opsane koule = 1.40126
  }
}
