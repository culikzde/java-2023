package tabappl;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class MyTable extends JTable {
    
    private DefaultTableModel model = null;
    private double [] [] original_array = null;
        
    public void displayArray (double [] [] a)
    {
        original_array = a;
        model = new DefaultTableModel ();
        
        int lines = a.length;
        int columns = 0;
        for (int i = 0; i < lines; i ++)
            if (a[i].length > columns)
                columns = a[i].length;
        
        for (int i = 0; i < columns; i ++)
        {
            char c = (char) ((int) 'A' + i);
            String name = "" + c;
            model.addColumn (name);
        }
        
        for (int i = 0; i < lines; i ++)
        {
           int len = a[i].length; 
            
           Object [] line = new Object [len];
           for (int k = 0; k < len; k ++)
               line [k] = a[i][k];
           
           model.addRow (line);
        }
        
        this.setModel (model);
        
         TableModelListener listener = new TableModelListener()    
         {
                @Override
                public void tableChanged (TableModelEvent evt) 
                {
                   myTableChanged (evt);        
                }
         };
         model.addTableModelListener(listener);
    }
    
    private void myTableChanged (TableModelEvent evt) 
    {
       if (evt.getType() == TableModelEvent.UPDATE)
       {
           int line = evt.getFirstRow();
           int col = evt.getColumn();
           Object obj = model.getValueAt(line, col);
           // setTitle ("UPDATE " + line + "," + col + " to " + obj);
           String s = obj.toString();
           double number = Double.valueOf (s);
           original_array [line][col] = number;
       }
    }
            
}
